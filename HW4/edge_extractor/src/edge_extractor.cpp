#include <ros/ros.h>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <cv_bridge/cv_bridge.h>
#include <sensor_msgs/image_encodings.h>

static const std::string OPENCV_WINDOW = "Image window";
static const std::string OPENCV_WINDOW_BW = "BWImage window";
static const std::string OPENCV_WINDOW_GAUSSIAN = "gaussianImage window";
static const std::string OPENCV_WINDOW_FILTER = "filteredImage window";

static const std::string OPENCV_WINDOW_BINARY = "binaryImage window";

void imgCallback(const sensor_msgs::CompressedImage& img)
{
    
    cv::namedWindow(OPENCV_WINDOW);
    cv::namedWindow(OPENCV_WINDOW_BW);
    cv::namedWindow(OPENCV_WINDOW_GAUSSIAN);
    cv::namedWindow(OPENCV_WINDOW_FILTER);

    cv::namedWindow(OPENCV_WINDOW_BINARY);


    cv_bridge::CvImagePtr cv = cv_bridge::toCvCopy(img, sensor_msgs::image_encodings::BGR8);
    
    // 1. showing the original image
    cv::imshow(OPENCV_WINDOW, cv->image);       
    cv::waitKey(10);

    // 2. converting the image to grayscale
    cv::Mat grayImg;
    cv::cvtColor( cv->image, grayImg, CV_BGR2GRAY );
    cv::imshow(OPENCV_WINDOW_BW, grayImg);
    cv::waitKey(10);

    // 3. Gaussian Blur
    cv::GaussianBlur( grayImg, grayImg, cv::Size( 3, 3 ), 0, 0 );
    cv::imshow(OPENCV_WINDOW_GAUSSIAN, grayImg);
    cv::waitKey(10);

//  Binary filter
    double  thresh=100;
		double  maxval=255;   //white pixel if value_pixel>thresh (or it will be black)
    cv::threshold( grayImg, grayImg, thresh, maxval, cv::THRESH_BINARY );
    cv::imshow(OPENCV_WINDOW_BINARY, grayImg);
    cv::waitKey(10);
//

    // 4. Canny corner detector
    int kernel_size=3;
    cv::Canny( grayImg, grayImg, /*190*/ 253, 254/*210*/, kernel_size );

    // 5. showing the resulting image
    cv::imshow(OPENCV_WINDOW_FILTER, grayImg);
    cv::waitKey(10);
}

int main(int argc, char** argv)
{
  ros::init(argc,argv,"filter");
  ros::NodeHandle n;
  
  ros::Subscriber sub = n.subscribe("/default/camera_node/image/compressed", 1, imgCallback);
  
  ros::spin();
}
