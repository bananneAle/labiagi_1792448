import tensorflow as tf
from keras.preprocessing.image import ImageDataGenerator
import matplotlib.pyplot as plt
import numpy as np
from keras.models import Sequential, load_model
from keras.layers import Dense, Dropout, Activation, Flatten, Conv2D, MaxPooling2D

train_datagen = ImageDataGenerator(
    rescale=1. / 255)

result = train_datagen.flow_from_directory(
    'DITS-full/DITS-classification/classification train',
    target_size=(50, 50),
    batch_size=7489,
    classes=None,
    class_mode='categorical')

test_datagen = ImageDataGenerator(
    rescale=1. / 255,
    shear_range=0.2,
    zoom_range=0.2,
    horizontal_flip=False)

test_result = train_datagen.flow_from_directory(
    'DITS-full/DITS-classification/classification test',
    target_size=(50, 50),
    batch_size=1159,
    classes=None,
    class_mode='categorical')


x_train = result[0][0]
y_train = result[0][1]
y_result = []

x_test = test_result[0][0]
y_test = test_result[0][1]
y_test_result = []

for lista in y_train:
	for i in range(59):
		if (lista[i] == 1):
			y_result.append(i)
			break


for lista in y_test:
	for i in range(59):
		if (lista[i] == 1):
			y_test_result.append(i)
			break

# creo il modello
model = Sequential()

model.add(Conv2D(32, (3, 3), input_shape=(50,50,3)))
model.add(Activation('relu'))
model.add(Dropout(0.25))

model.add(Conv2D(16, (3, 3)))
model.add(Activation('relu'))
model.add(Dropout(0.25))

model.add(Flatten())

model.add(Dense(59))
model.add(Activation('softmax'))

model.compile(optimizer='adam',
              loss='sparse_categorical_crossentropy',
              metrics=['accuracy'])


history = model.fit(x_train, y_result, epochs=7, validation_data=(x_test, y_test_result))

model.save('cartelli.model')

new_model = tf.keras.models.load_model('cartelli.model')
loss_and_metrics = new_model.evaluate(x_test, y_test_result, verbose=2)

print("Test Loss", loss_and_metrics[0])
print("Test Accuracy", loss_and_metrics[1])

new_model = tf.keras.models.load_model('cartelli.model')
predicted_classes = new_model.predict_classes(x_test)


# see which we predicted correctly and which not
correct_indices = np.nonzero(predicted_classes == y_test_result)[0]
incorrect_indices = np.nonzero(predicted_classes != y_test_result)[0]

print()
print(len(correct_indices)," classified correctly")
print(len(incorrect_indices)," classified incorrectly")

# adapt figure size to accomodate 18 subplots

fig = plt.figure()
plt.subplot(2,1,1)
plt.plot(history.history['accuracy'])
plt.plot(history.history['val_accuracy'])
plt.title('model accuracy')
plt.ylabel('accuracy')
plt.xlabel('epoch')
plt.legend(['train', 'test'], loc='lower right')

plt.subplot(2,1,2)
plt.plot(history.history['loss'])
plt.plot(history.history['val_loss'])
plt.title('model loss')
plt.ylabel('loss')
plt.xlabel('epoch')
plt.legend(['train', 'test'], loc='upper right')

plt.tight_layout()


plt.rcParams['figure.figsize'] = (7,14)

figure_evaluation = plt.figure()

# plot 9 correct predictions
for i, correct in enumerate(correct_indices[:9]):
    plt.subplot(6,3,i+1)
    plt.imshow(x_test[correct], cmap='gray', interpolation='none')
    plt.title("Predicted: {}, Truth: {}".format(predicted_classes[correct], y_test_result[correct]))
    plt.xticks([])
    plt.yticks([])

# plot 9 incorrect predictions
for i, incorrect in enumerate(incorrect_indices[:9]):
    plt.subplot(6,3,i+10)
    plt.imshow(x_test[incorrect], cmap='gray', interpolation='none')
    plt.title("Predicted {}, Truth: {}".format(predicted_classes[incorrect], y_test_result[incorrect]))
    plt.xticks([])
    plt.yticks([])

plt.show()
plt.savefig("incorrect"+str(i)+".ps")
figure_evaluation

