#include <ros/ros.h>
#include <actionlib/client/simple_action_client.h>

#include <move_base_msgs/MoveBaseAction.h> // Note: "Action" is appended
#include "nav_msgs/Odometry.h"


typedef actionlib::SimpleActionClient<move_base_msgs::MoveBaseAction> MoveBaseClient;

void initPoseCallback(const nav_msgs::Odometry& msg) {

    ROS_INFO("Starting callback");

    MoveBaseClient ac("move_base", true);
    
    //aspetto il server server
    while (!ac.waitForServer(ros::Duration(5.0)))
    {
        ROS_INFO("Waiting for the move_base action server to come up");
    }
    
    //creo la variabile per il goal
    move_base_msgs::MoveBaseGoal goal;
    //setto un goal
    goal.target_pose.header.frame_id = "base_link";
    goal.target_pose.header.stamp = ros::Time::now();
    goal.target_pose.pose.position.x = -15;
    goal.target_pose.pose.position.y = 22;
    goal.target_pose.pose.position.z = 0;
    goal.target_pose.pose.orientation.x = 0;
    goal.target_pose.pose.orientation.y = 0;
    goal.target_pose.pose.orientation.z = 0;
    goal.target_pose.pose.orientation.w = 6e-17;

    //mando il goal
    ROS_INFO("Sending goal");
    ac.sendGoal(goal);
    
    //aspetto 10 secondi
    sleep(10);

    //cancella tutti i goal correnti
    ac.cancelAllGoals();

    //Metto le posizioni che pubblico
    goal.target_pose.header.frame_id = "base_link";
    goal.target_pose.header.stamp = ros::Time::now();
    msg.pose.pose.position.x;
    goal.target_pose.pose.position.x = msg.pose.pose.position.x;
    goal.target_pose.pose.position.y = msg.pose.pose.position.y;
    goal.target_pose.pose.position.z = msg.pose.pose.position.z;
    goal.target_pose.pose.orientation.x = msg.pose.pose.orientation.x;
    goal.target_pose.pose.orientation.y = msg.pose.pose.orientation.y;
    goal.target_pose.pose.orientation.z = msg.pose.pose.orientation.z;
    goal.target_pose.pose.orientation.w = msg.pose.pose.orientation.w;

    //sono arrivato al goal
    ROS_INFO("Sending goal to reach initial position");
    ac.sendGoal(goal);

    //aspetto per i risultati
    ac.waitForResult();
    ros::shutdown();
    return;
}

int main(int argc, char **argv)
{
    ros::init(argc, argv, "move_base_client_node");
    ros::NodeHandle n;

    //mi iscrivo a initialpose
	ros::Subscriber initPose_sub = n.subscribe("/base_pose_ground_truth", 1, initPoseCallback);
    ros::spin();

    return 0;
}
