
 # include <ros/ros.h>
 # include <math.h>
 #include <stdlib.h>
 #include "std_msgs/String.h"

 # include <turtlesim/SpawnCircle.h>
 # include <turtlesim/RemoveCircle.h>
 # include <turtlesim/GetCircles.h>
 # include <turtlesim/Pose.h>

 # define TWIDTH 0.5f

void chatterCallback(turtlesim::Pose msg)
{
  //Inizializzo il NodeHandle e i Service
  ros::NodeHandle n;
  ros::ServiceClient client = n.serviceClient<turtlesim::RemoveCircle>("remove_circle");
  ros::ServiceClient client2 = n.serviceClient<turtlesim::GetCircles>("get_circles");
  turtlesim::RemoveCircle rmv;
  turtlesim::GetCircles get;

  //Mi salvo su delle variabili i parametri attuali del topic ovvero le tartarughe in movimento
  float tX=msg.x;
  float tY=msg.y;

  //Chiamo get circle e se non da errore continuo
  if (client2.call(get))
    {
      //Mi creo un vettore per contenere i circles della response
      std::vector<turtlesim::Circle> circles = get.response.circles;

      //Scorro per tutti i cerchi per vedere se verifica la condizione successiva almeno per uno
      for(int i=0; i < circles.size(); i++)
        {
          //Mi salvo su delle variabili le coordinate del cerchio che sto analizzando
          float cX = circles[i].x;
          float cY = circles[i].y;

          //Teorema di pitagora per sapere la distanza tra le due tartarughe
          float dist = sqrt(pow(tX-cX,2)+pow(tY-cY,2));
          //Se la distanza è in questo caso minore o uguale ad 1 levo il cerchio e la tartaruga collegata
          if(dist <= TWIDTH*2)
            {
              //Mi preparo i dati da dare in pasto alla call del remove
              rmv.request.id = circles[i].id;

              //Faccio una removeCircle e se è andata a buon fine printo il nuovo vector di circle
              if (client.call(rmv))
                {
                  printf("Circles: [");
                  for(int j=0; j < rmv.response.circles.size(); j++)
                    {
                      if(j==rmv.response.circles.size()-1)
                        printf("(%d, %.2f, %.2f)", rmv.response.circles[j].id, rmv.response.circles[j].x, rmv.response.circles[j].y);
                      else
                        printf("(%d, %.2f, %.2f), ", rmv.response.circles[j].id, rmv.response.circles[j].x, rmv.response.circles[j].y);
                    }
                  printf("]\n");
                }
              else
                {
                  ROS_ERROR("Failed to call service remove_circle (client)");
                }
            }
        }
    }
  else
    {
      ROS_ERROR("Failed to call service get_circle (client)");
    }
}



int main(int argc, char **argv)
{
  //Inizializzo il nodo remove_circle_client e il NodeHandle
  ros::init(argc, argv, "remove_circle_client");
  ros::NodeHandle n;

  //Se ha argomenti cancella gli id che gli passo come argomento
  if(argc > 1)
    {
      //Invoco il service del server per RemoveCircle con request  id uguale all'args
      ros::ServiceClient client = n.serviceClient<turtlesim::RemoveCircle>("remove_circle");
      turtlesim::RemoveCircle srv;
      int id = atoll(argv[1]);
      srv.request.id = id;

      //Se riesco a rimuovere il cerchio stampo come richiesto da specifica
      if (client.call(srv))
        {
          printf("Circles: [");
          for(int j=0; j < srv.response.circles.size(); j++)
            {
              if(j==srv.response.circles.size()-1)
                printf("(%d, %.2f, %.2f)", srv.response.circles[j].id, srv.response.circles[j].x, srv.response.circles[j].y);
              else
                printf("(%d, %.2f, %.2f), ", srv.response.circles[j].id, srv.response.circles[j].x, srv.response.circles[j].y);
            }
          printf("]\n");
        }

      //Se invece non ci riesce allora  mando un messaggio d'errore
      else
        {
          ROS_ERROR("Failed to call service remove_circle (client)");
          return true;
        }
    }

  //Se non ha argomenti distrugge le tartarughe al contatto, facendo lo spin di un subscriber con la funzione chatterCallback
  else
    {
      ros::Subscriber sub = n.subscribe("turtle1/pose", 1, chatterCallback);
      ros::spin();
    }
  return false;
}