 # include <time.h>
 # include <stdlib.h>

 # include <ros/ros.h>

 # include <turtlesim/SpawnCircle.h>
 # include <turtlesim/RemoveCircle.h>
 # include <turtlesim/GetCircles.h>

 # define HEIGHT 12
 # define WIDTH  12

int main(int argc, char **argv)
{
  //Imposto il seed con il tempo per far si che sia il più random possibile
  srand(time(NULL));

  //Inizializzo il node client_spawn
  ros::init(argc, argv, "spawn_circle_client");

  //Se il comando è stato usato impropriamente mando un ROS_ERROR su come si utilizza il comando e mando in errore
  if (argc != 2)
    {
      ROS_ERROR("Command usage: circle_spawn N (number of circle)");
      return true;
    }

  //Inizializzo il NodeHandle e il ServiceClient
  ros::NodeHandle n;
  ros::ServiceClient client = n.serviceClient<turtlesim::SpawnCircle>("spawn_circle");

  //Mi prendo la variabile N da gli argv del comando e la utilizzeremo per spawnare N turtle
  int N = atoll(argv[1]);
  for(int i=0; i<N; i++)
    {
      //inizializzo delle x e y random
      float x=(float)rand()/(float)(RAND_MAX/HEIGHT);
      float y=(float)rand()/(float)(RAND_MAX/WIDTH);

      //Faccio la request al server per spawnare la turtle corrente
      turtlesim::SpawnCircle srv;
      srv.request.x = x;
      srv.request.y = y;

      //Se tutto va bene
      if (client.call(srv))
        {
          //Stampo informazioni sulla nuova tartaruga
          ROS_INFO("Turtle Spawned info: name=%s,x=%.2f,y=%.2f", ("turtle"+std::to_string(srv.response.circles[srv.response.circles.size()-1].id)).c_str(), x, y);
          //Stampo tutti i cerchi
          printf("Circles: [");
          for(int j=0; j < srv.response.circles.size(); j++)
            {
              if(j==srv.response.circles.size()-1)
                printf("(%d, %.2f, %.2f)", srv.response.circles[j].id, srv.response.circles[j].x, srv.response.circles[j].y);
              else
                printf("(%d, %.2f, %.2f), ", srv.response.circles[j].id, srv.response.circles[j].x, srv.response.circles[j].y);
            }
          printf("]\n");
        }
      //Se invece va male
      else
        {
          ROS_ERROR("Failed to call service spawn_circle (client)");
          return true;
        }
    }
  return false;
}
