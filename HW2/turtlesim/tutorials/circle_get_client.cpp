 # include <ros/ros.h>

 # include <turtlesim/SpawnCircle.h>
 # include <turtlesim/RemoveCircle.h>
 # include <turtlesim/GetCircles.h>

int main(int argc, char **argv)
{
  //Inizializzo il node client_get_circle
  ros::init(argc, argv, "get_circle_client");

  //Inizializzo il NodeHandle e il ServiceClient
  ros::NodeHandle n;
  ros::ServiceClient client = n.serviceClient<turtlesim::GetCircles>("get_circles");

  //Inizializzo il srv
  turtlesim::GetCircles srv;

  //Se tutto va bene
  if (client.call(srv))
    {
      //Stampo i cerchi in maniera ordinata
      printf("Circles: [");
      for(int j=0; j < srv.response.circles.size(); j++)
        {
          if(j==srv.response.circles.size()-1)
            printf("(%d, %.2f, %.2f)", srv.response.circles[j].id, srv.response.circles[j].x, srv.response.circles[j].y);
          else
            printf("(%d, %.2f, %.2f), ", srv.response.circles[j].id, srv.response.circles[j].x, srv.response.circles[j].y);
        }
      printf("]\n");
    }

  //Se invece va male
  else
    {
      ROS_ERROR("Failed to call service get_circle (client)");
      return true;
    }

  return false;
}
