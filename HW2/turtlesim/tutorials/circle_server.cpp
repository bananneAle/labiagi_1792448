# include <string.h>
# include <string>
# include <stdlib.h>
# include <cstdlib>
# include <time.h>

# include <ros/ros.h>

# include <turtlesim/SpawnCircle.h>
# include <turtlesim/RemoveCircle.h>
# include <turtlesim/GetCircles.h>
# include <turtlesim/Spawn.h>
# include <turtlesim/Kill.h>

//Vector di circles viene poi convertito in Circle[] quando va restituito
std::vector<turtlesim::Circle> circles;



//Servizio in risposta allo spawn_circle
bool spawn_circle (turtlesim::SpawnCircle::Request &req,
                  turtlesim::SpawnCircle::Response &res)
{
  //Preparo i dati da mandare a Spawn
  ros::NodeHandle n;
  ros::ServiceClient client = n.serviceClient<turtlesim::Spawn>("spawn");
  turtlesim::Spawn srv;
  srv.request.x = req.x;
  srv.request.y = req.y;
  srv.request.theta = rand()%360;
  //Se tutto va bene
  if (client.call(srv))
  {
          //Creo una stringa nella quale mi salvo il nome generato da spawn
          std::string nome=srv.response.name;
          //Estrapolo l'id dal nome della tartaruga e lo converto sapendo che viene generato del tipo turtle... (dove i puntini sono un numero)
          int id=std::stoi(nome.substr(6,nome.length()-6));
          //Stampo la ROS_INFO con il nome
          ROS_INFO("\nTurtle: %s", nome.c_str());

          //creo un cerchio alle cordinate della req
          turtlesim::Circle circle;
          circle.id = id;
          circle.x = req.x;
          circle.y = req.y;
          circles.push_back(circle);
          res.circles = circles;
          //Stampo la ROS_INFO con dei dati sul cerchio appena spawnato
          ROS_INFO("Spawned circle at: x=%.2f, y=%.2f", req.x, req.y);
  }
  //Se invece va male
  else
  {
          ROS_ERROR("Failed to call spawn_circle (turtle)");
          return false;
  }
  return true;
}



//Servizio in risposta al remove_circle
bool remove_circle  (turtlesim::RemoveCircle::Request &req,
                    turtlesim::RemoveCircle::Response &res)
{
  //Rimuovo il cerchio con req.id e mando un ROS_INFO
  ROS_INFO("Removing circle with id=%d", req.id);

  //Faccio un for finchè non trovo l'indice che mi interessa
  for (int i=0; i<circles.size(); i++){
    if(circles[i].id == req.id)
      //Uso i metodi di vector per eliminare l'elemento i
      circles.erase(circles.begin() + i);
  }

  //Mando in response il vector di circles aggiornato
  res.circles = circles;

  //Inizializzo il NodeHandle e il ServiceClient
  ros::NodeHandle n;
  ros::ServiceClient client = n.serviceClient<turtlesim::Kill>("kill");

  //Chiamo la funzione kill di turtlesim con turtle+id
  turtlesim::Kill srv;
  srv.request.name = "turtle" + std::to_string(req.id);
  //Se tutto va bene
  if (client.call(srv))
  {
          ROS_INFO("Turtle removed");
  }

  //Se qualcosa va storto
  else
  {
          ROS_ERROR("Failed to call service remove_circle");
          return false;
  }
  return true;
}



//Servizio in risposta al get_circles
bool get_circles  (turtlesim::GetCircles::Request &req,
                  turtlesim::GetCircles::Response &res)
{
  res.circles = circles;
  return true;
}



//main nel quale faccio l'advertise dei servizi e nel quale inizializzo il nodo
int main(int argc, char **argv)
{
  //Imposto il seed con il tempo per far si che sia il più random possibile
  srand (time(NULL));
  //Creo il nodo circles_server
  ros::init(argc, argv, "circles_server");
  //Aggiungo un NodeHandle
  ros::NodeHandle n;

  //Faccio l'advertise di tutti i servizi dai quali posso ricevere richieste
  ros::ServiceServer service_1 = n.advertiseService("spawn_circle", spawn_circle);
  ROS_INFO("Ready to spawn circle.");
  ros::ServiceServer service_2 = n.advertiseService("remove_circle", remove_circle);
  ROS_INFO("Ready to remove circle.");
  ros::ServiceServer service_3 = n.advertiseService("get_circles", get_circles);
  ROS_INFO("Ready to get circles.");

  //Cicla il main
  ros::spin();

  return 0;
}
